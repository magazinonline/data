<?php
include "parts/head.php";
include "parts/menu.php";

$product = new Product($_GET['id']);
?>

<div class="row">
    <div class="col-9 text-center">
        <div class="row">
            <div class="col text-center">
                <?php foreach ($product->getProductImages() as $image): ?>
                    <div><img width="300" src="images/<?php echo $image->url;?>" /> </div>
                <?php endforeach;?>
                <p>  <?php echo $product->name;?> </p>
                <p> PRET: <?php echo $product->getFinalPrice();?> </p>
                <p>   <?php echo $product->description;?> </p>
                <br />
            </div>
        </div>
    </div>
            <div class="col">

                <h3 style="color:green" class="text-center" > <em> NU GASESTI NICIUNDE MAI IEFTIN!</em> </h3>
                <img src="images/electrocasnice1.png" width="200" class="rounded">
            </div>
</div>
<?php include "parts/footer.php" ?>