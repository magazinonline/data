<?php
include "config.php";

function dbDelete($table, $id){
    global $dbConnection;
    $result = mysqli_query($dbConnection, "DELETE FROM $table WHERE id=".intval($id));

    return mysqli_affected_rows($dbConnection)>0;
}

function dbInsert($table, $data)
{
    global $dbConnection;
    $columns = [];
    $values = [];

    foreach ($data as $column => $value) {
        $columns[] = "`" . mysqli_real_escape_string($dbConnection, $column) . "`";
        $values[] = "'" . mysqli_real_escape_string($dbConnection, $value) . "'";
    }
    $sqlcolumns = implode(',', $columns);
    $sqlvalues = implode(',', $values);
    $result = mysqli_query($dbConnection, "INSERT INTO $table($sqlcolumns) VALUES ($sqlvalues)");
    return mysqli_insert_id($dbConnection);
}


function dbUpdate($table, $id, $data){
    global $dbConnection;
    $sets = [];
    foreach ($data as $column => $value){
        $sets[]="`".mysqli_real_escape_string($dbConnection,$column)."`='".mysqli_real_escape_string($dbConnection,$value)."'";
    }
    $sqlSets = implode(',', $sets);

    $result = mysqli_query($dbConnection, "UPDATE $table SET $sqlSets  WHERE id=".intval($id));

    return mysqli_affected_rows($dbConnection)>0;

}

function dbSelectOne($table, $filters=[], $likeFilters=[], $offset=0, $limit=null,  $sortBy=null, $sortDirection='ASC'){
    $data = dbSelect($table, $filters, $likeFilters, 0, 1,  $sortBy, $sortDirection);
    if (isset($data[0])){
        return $data[0];
    } else {
        return null;
    }

}

/**
 *  select all -
 *  select by -  WHERE user_id=5 AND active=1
 *  select like -
 *  limit
 *  offset
 *  sorting
 */
function dbSelect($table, $filters=[], $likeFilters=[], $offset=0, $limit=null,  $sortBy=null, $sortDirection='ASC'){
    global $dbConnection;
    $sql = "SELECT * FROM $table";

    if ($filters!=null || $likeFilters!=null){
        $sets = [];
        foreach ($filters as $column => $value){
            $sets[]="`".mysqli_real_escape_string($dbConnection,$column)."`='".mysqli_real_escape_string($dbConnection,$value)."'";
        }
        foreach ($likeFilters as $column => $value){
            $sets[]="`".mysqli_real_escape_string($dbConnection,$column)."` LIKE '%".mysqli_real_escape_string($dbConnection,$value)."%'";
        }
        $sql.= ' WHERE '.implode(' AND ', $sets);
    }

    if ($sortBy != null) {
        $sql.= ' ORDER BY '.mysqli_real_escape_string($dbConnection,$sortBy).' '.mysqli_real_escape_string($dbConnection,$sortDirection);
    }

    if ($limit!=null){
        $sql.= ' LIMIT '.intval($offset).','.intval($limit);
    }

    $result = mysqli_query($dbConnection, $sql);

    if (!$result){
        die("SQL error: " . mysqli_error($dbConnection)." SQL:".$sql);
    }

    return $result->fetch_all(MYSQLI_ASSOC);
}