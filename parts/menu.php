<?php
include "lib/BaseEntity.php";
include "lib/Product.php";
include "lib/Category.php";
include "lib/ProductImage.php";

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="index.php">Tech your home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Categorii
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <?php
                $categoryData=dbSelect('category',[],[]);
                foreach ($categoryData as $categoryItem):?>
                    <?php $category=new Category($categoryItem['id']);?>
                    <a class="dropdown-item" href="categorie.php?id=<?php echo $category->id;?>"><?php echo $category->name;?></a>
                <?php endforeach;?>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Contact</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Informatii utile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="login.php">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="cart.php"><img src="images/cart.jpg" width="20"></a>
            </li>

        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>