<?php

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 8/1/2019
 * Time: 8:12 PM
 */
class ProductImage
{
    public $id;
    public $url;
    public $product_id;

    public function __construct($id)
    {
        $data = dbSelectOne('product_images',['id'=>$id]);
        foreach ($data as $key => $value){
            $this->$key = $value;
        }
    }
}